package com.example.czf.firecrud.modelo;

/**
 * Created by CZF on 15/05/2018.
 */

public class Pergunta {

    private String uid;

    private String titulo;

    private String desc;



    public Pergunta() {
        this.uid = uid;
        this.titulo = titulo;
        this.desc = desc;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return titulo;
    }
}
