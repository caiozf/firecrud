package com.example.czf.firecrud;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.czf.firecrud.modelo.Pergunta;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    boolean editing = false;

    EditText edtPergunta, edtDesc;

    ListView listV_dados;

    FirebaseDatabase firebaseDatabase;

    DatabaseReference databaseReference;

    private List<Pergunta> listPergunta = new ArrayList<Pergunta>();

    private ArrayAdapter<Pergunta> arrayAdapterPergunta;

    Pergunta perguntaSelecionada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtDesc = (EditText) findViewById(R.id.editDesc);

        edtPergunta = (EditText) findViewById(R.id.editPergunta);

        listV_dados = (ListView) findViewById(R.id.listV_dados);

        inicializarFirebase();

        listarPerguntas();

        listV_dados.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                perguntaSelecionada = (Pergunta)adapterView.getItemAtPosition(position);
                AlertDialog.Builder modal = new AlertDialog.Builder(MainActivity.this);

                    modal.setMessage("Pergunta: "
                            + System.getProperty ("line.separator") + System.getProperty ("line.separator")
                            + perguntaSelecionada.getTitulo()
                            + System.getProperty ("line.separator") + System.getProperty ("line.separator")
                            + "Descricao: "
                            + System.getProperty ("line.separator") + System.getProperty ("line.separator")
                            + perguntaSelecionada.getDesc()
                            + System.getProperty ("line.separator"))

                            .setPositiveButton(
                                    "Editar",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            edtPergunta.setText(perguntaSelecionada.getTitulo());

                                            edtDesc.setText(perguntaSelecionada.getDesc());

                                            editing = true;

                                        }
                                    })
                            .setNegativeButton("Fechar",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    })
                            .create()
                            .show();
            }
        });

    }

    private void inicializarFirebase(){
        FirebaseApp.initializeApp(MainActivity.this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseDatabase.setPersistenceEnabled(true);
        databaseReference = firebaseDatabase.getReference();
    }

    private void listarPerguntas(){
        databaseReference.child("Pergunta").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listPergunta.clear();

                for(DataSnapshot objSnapshot: dataSnapshot.getChildren()){
                    Pergunta p = objSnapshot.getValue(Pergunta.class);
                    listPergunta.add(p);
                }
                arrayAdapterPergunta = new ArrayAdapter<Pergunta>(MainActivity.this,
                                                                    android.R.layout.simple_list_item_1,
                                                                    listPergunta);
                listV_dados.setAdapter(arrayAdapterPergunta);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id == R.id.menu_novo){

            if (edtPergunta.getText().toString().matches("") || edtDesc.getText().toString().matches("")) {
               makeToast("Preencha a pergunta e a descricao!");
            }else if(editing){
                makeToast("Nao pode adicionar pergunta repitida");
            }else{
                Pergunta p = new Pergunta();

                p.setUid(UUID.randomUUID().toString());
                p.setTitulo(edtPergunta.getText().toString());
                p.setDesc(edtDesc.getText().toString());

                databaseReference.child("Pergunta").child(p.getUid()).setValue(p);

                limparCampos();
                String message = "Pergunta adicionada com sucesso!";
                makeToast(message);
            }
        }else if(id == R.id.menu_atualiza){

            if (edtPergunta.getText().toString().matches("") || edtDesc.getText().toString().matches("")) {
                makeToast("Selecione uma pergunta que deseja editar");
            }else{
                Pergunta p = new Pergunta();

                p.setUid(perguntaSelecionada.getUid());
                p.setTitulo(edtPergunta.getText().toString().trim());
                p.setDesc(edtDesc.getText().toString().trim());
                databaseReference.child("Pergunta").child(p.getUid()).setValue(p);

                limparCampos();
                editing = false;
                String message = "Pergunta atualizada com sucesso!";
                makeToast(message);
            }
        }else if(id == R.id.menu_excluir){


            AlertDialog.Builder modal = new AlertDialog.Builder(MainActivity.this);
            if (edtPergunta.getText().toString().matches("") || edtDesc.getText().toString().matches("")) {
                makeToast("Selecione uma pergunta que deseja editar");
            }else{
                modal.setMessage("Deseja realmente excluir?")

                        .setPositiveButton(
                                "SIM",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Pergunta p = new Pergunta();
                                        p.setUid(perguntaSelecionada.getUid());
                                        databaseReference.child("Pergunta").child(p.getUid()).removeValue();
                                        String message = "Pergunta removida com sucesso";
                                        makeToast(message);
                                        limparCampos();
                                        editing = false;
                                    }
                                })
                        .setNegativeButton("Cancelar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                })
                        .create()
                        .show();
            }

        }

        return true;
    }

    private void limparCampos(){
        edtPergunta.setText("");
        edtDesc.setText("");
    }

    private  void makeToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
